context('KazanExpress', () => {
  

  describe('Checking interface unauthorised', () => {

    beforeEach(() => {
      cy.visit('https://kazanexpress.ru/')
    })


    it('Check wishes', () => {
      cy.get('#wishes-button').click()
      cy.contains('Мои желания')
      cy.contains('Здесь пусто :(')
      cy.contains('У Вас пока нет желаний.')
      cy.contains('Вернуться на главную').click()
    })

    it('Check login', () => {
      cy.get('[id="signin-button"]').click()
      cy.get('[placeholder="E-mail или телефон"]').type('Abdul@xitrii.ru')
      cy.get('[placeholder="Пароль"]').type('somestuff')
      cy.get('.noselect.solid.wide').click()
      cy.get('.has-error')

    })

    it('Check basket', () => {
      cy.get('#cart-button').click()
      cy.contains('Ваша корзина')
      cy.contains('Здесь пусто :(')
      cy.contains('В Корзину ничего не добавлено ')
      cy.contains('Воспользуйтесь поиском, чтобы найти всё что нужно. ')
      cy.contains('Начать покупки').click()
    })

  })
})
